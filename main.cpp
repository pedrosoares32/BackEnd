#ifndef TIGREFRAMEWORK_MAIN_H
#define TIGREFRAMEWORK_MAIN_H

#include <iostream>
#include <TigreFramework/Core/Kernel/Request.h>
#include <App/Console/ConsoleHandle.h>
#include <Types/Date.h>
#include <Http/Request.h>
#include <nlohmann/json.hpp>
#include <App/Entities/Cotation.h>
#include <Types/Math.h>


using namespace std;

int main(int argc, char* argv[]) {
    /*putenv((char*)"TZ=America/Anguilla");
    Http::Request request;
    nlohmann::json data = nlohmann::json::parse(request.perform("https://api.bitfinex.com/v2/candles/trade:1D:tIOTBTC/hist?start=1513216800000"));

    for(auto candle : data){
        Date *date = new Date((long int)candle[0] / 1000);
        std::cout << date->formatUTC("%d/%m/%Y %H:%M:%S") << " HIGH: " << candle[3] << " LOW: " << candle[4] << std::endl;

        Cotation currentMarket;
        currentMarket.lower = (candle[4]);
        currentMarket.higher = (candle[3]);
        currentMarket.coin_id = 7;
        currentMarket.date = *Date::fromFormat("%d/%m/%Y %h:%i:%s", date->formatUTC("%d/%m/%Y") + "00:00:00");
        currentMarket.save();

        delete date;
    }
    return 0;*/

    if(argc == 1){
        Request r = Request();
        try{
            r.loadApp();
        } catch(const std::runtime_error& re) {
            cout << r.header->getHeader();
            cout << "\r\n"; // Separe Header From HTML Site
            std::cout << "Runtime error: " << re.what() << std::endl;
        } catch(const std::exception& ex)  {
            cout << r.header->getHeader();
            cout << "\r\n"; // Separe Header From HTML Site
            std::cout << "Error occurred: " << ex.what() << std::endl;
        } catch (...) {
            cout << r.header->getHeader();
            cout << "\r\n"; // Separe Header From HTML Site
            cout << "Deu Pau" << endl;
        }
    }else{
        std::vector<std::string> params;
        for(int i=1; i < argc; i++){
            std::string param = argv[i];
            params.push_back(param);
        }

        ConsoleHandle consoleHandle;
        consoleHandle.index(params);
    }

    return 0;
}

#endif //TIGREFRAMEWORK_MAIN_H