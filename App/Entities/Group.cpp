//
// Created by pedro on 10/01/18.
//

#include <Types/Math.h>
#include "Group.h"

std::vector<Group> Group::getGroupsCoins(User user) {
    std::string sql = PostgreSQL::prepare("select c.*, qg.group_id as group_id, g.name as group_name, g.buy_value as buy_value from coin c "
                                                  "join group_has_coin gc on gc.coin_id = c.id "
                                                  "join quota_has_group qg on qg.group_id = gc.group_id "
                                                  "join \"group\" g on g.id = qg.group_id "
                                                  "join user_has_quota uq on uq.quota_id = qg.quota_id "
                                                  "join users u on u.id = uq.user_id "
                                                  "WHERE u.id = ?;", { std::to_string(user.id) });

    PostgreSQL *connection = new PostgreSQL("localhost", 5432, "CryptoHolders");
    auto result = connection->execute(sql);

    std::vector<Group> groups;

    int activeGroup = -1;
    double cotacao_de_compra = -1;
    std::string activeGroupName = "";
    std::vector<Coin> coins;
    for(auto line : result->fetchAll()){
        if(activeGroup == -1){
            activeGroup = Math::toInt(line["group_id"]);
        }

        activeGroupName = line["group_name"];
        cotacao_de_compra = Math::toDouble(line["buy_value"]);

        if(activeGroup != Math::toInt(line["group_id"])){
            Group group;
            group.id = activeGroup;
            group.name = activeGroupName;
            group.cotacao_de_compra = cotacao_de_compra;
            group.coins = coins;
            group.coinsQuantity = Coin::getUserCoinQuantity(user, &group);
            groups.push_back(group);

            coins = std::vector<Coin>();
            activeGroup = Math::toInt(line["group_id"]);
        }

        Coin coin;
        coin.id = Math::toInt(line["id"]);
        coin.name = line["name"];
        coin.acronym = line["acronym"];
        coins.push_back(coin);
    }
    Group group;
    group.id = activeGroup;
    group.name = activeGroupName;
    group.cotacao_de_compra = cotacao_de_compra;
    group.coins = coins;
    group.coinsQuantity = Coin::getUserCoinQuantity(user, &group);
    groups.push_back(group);

    return groups;
}
