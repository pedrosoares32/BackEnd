//Cotation
// Created by pedro on 19/12/17.
//

#ifndef TIGREFRAMEWORK_COTATION_H
#define TIGREFRAMEWORK_COTATION_H


#include <Types/Date.h>
#include <vector>
#include <nlohmann/json.hpp>

class Cotation {

    public:
        Cotation();
        ~Cotation();

        static std::vector<Cotation> getCotation(int coin_id);
        void save();

        int coin_id;
        double lower;
        double higher;
        Date date;

};

void to_json(nlohmann::json &j, const Cotation &p);



#endif //TIGREFRAMEWORK_COTATION_H
