//
// Created by pedro on 12/12/17.
//

#include <TigreFramework/Core/Kernel/Request.h>
#include <vendor/Types/Math.h>
#include "User.h"
#include <Types/helper.h>
#include <nlohmann/json.hpp>

vector<User> User::all() {
    std::string sql = PostgreSQL::prepare("SELECT * FROM ?;", { "users" });
    PostgreSQL *connection = new PostgreSQL("localhost", 5432, "CryptoHolders");
    auto result = connection->execute(sql);
    std::vector<User> result_;

    for(auto line : result->fetchAll()){
        User model;
        model.id         = Math::toInt(line["id"]);
        model.name       = line["name"];
        model.password   = line["password"];
        model.email      = line["email"];
        model.key        = line["key"];
        model.secret     = line["secret"];

        result_.push_back(model);
    }

    return result_;
}

bool User::getUserByEmail(std::string email) {
    std::string sql = PostgreSQL::prepare("SELECT * FROM users WHERE email = ?;", { email });
    PostgreSQL *connection = new PostgreSQL("localhost", 5432, "CryptoHolders");
    auto result = connection->execute(sql);

    if(result->size() > 1 || result->size() == 0)
        return false;

    for(auto line : result->fetchAll()){
        this->id = Math::toInt(line["id"]);
        this->name = line["name"];
        this->password = line["password"];
        this->email = line["email"];


        return true;
    }
    return false;
}