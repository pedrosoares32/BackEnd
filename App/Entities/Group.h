//
// Created by pedro on 10/01/18.
//

#ifndef TIGREFRAMEWORK_GROUP_H
#define TIGREFRAMEWORK_GROUP_H


#include <vector>
#include <map>
#include <string>
#include "User.h"
#include "Coin.h"

class Group {

    public:
        static std::vector<Group> getGroupsCoins(User user);

        int id;
        std::string name;
        double cotacao_de_compra;
        std::vector<Coin> coins;
        std::map<std::string, double> coinsQuantity;

};


#endif //TIGREFRAMEWORK_GROUP_H
