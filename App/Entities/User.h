//
// Created by pedro on 12/12/17.
//

#ifndef TIGREFRAMEWORK_USER_H
#define TIGREFRAMEWORK_USER_H

#include <TigreFramework/PostgreSQLConnector/PostgreSQL.h>
#include <string>

class User {

    public:
        int id;
        std::string name;
        std::string email;
        std::string password;

        vector<User> all();

        bool getUserByEmail(std::string email);

    private:
        std::string key;
        std::string secret;

        int ClientID;
        int BTC;
        int BRL;

};


#endif //TIGREFRAMEWORK_USER_H
