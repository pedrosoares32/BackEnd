//
// Created by pedro on 15/12/17.
//

#include <Types/Math.h>
#include <Types/helper.h>
#include <nlohmann/json.hpp>
#include <Http/Request.h>
#include "Coin.h"
#include "Group.h"

std::vector<Cotation> Coin::getCotation() {
    std::vector<Cotation> cotation = Cotation::getCotation(this->id);
    Coin coin = Coin::first(cotation.front().coin_id);
    cotation.push_back(coin.getCurrentCotation());
    return cotation;
}

std::vector<Coin> Coin::getUserCoin(User &user) {
    std::string sql = PostgreSQL::prepare("select c.* from coin c "
                                          "join group_has_coin gc on gc.coin_id = c.id "
                                          "join quota_has_group qg on qg.group_id = gc.group_id "
                                          "join user_has_quota uq on uq.quota_id = qg.quota_id "
                                          "join users u on u.id = uq.user_id "
                                          "WHERE u.id = ?;", { std::to_string(user.id) });

    PostgreSQL *connection = new PostgreSQL("localhost", 5432, "CryptoHolders");
    auto result = connection->execute(sql);

    std::vector<Coin> coins;

    for(auto line : result->fetchAll()){
        Coin coin;
        coin.id = Math::toInt(line["id"]);
        coin.name = line["name"];
        coin.acronym = line["acronym"];

        coins.push_back(coin);
    }
    return coins;
}

Coin Coin::first(int id) {
    std::string sql = PostgreSQL::prepare(
            "SELECT id, name, LOWER(acronym) as acronym FROM \"coin\" as c WHERE c.id = ?;", { id }
    );

    PostgreSQL *connection = new PostgreSQL("localhost", 5432, "CryptoHolders");
    auto result = connection->execute(sql);

    std::vector<Coin> coins;

    for(auto line : result->fetchAll()){
        Coin coin;
        coin.id = Math::toInt(line["id"]);
        coin.name = line["name"];
        coin.acronym = line["acronym"];

        return coin;
    }
    return Coin();
}

std::map<std::string, double> Coin::getUserCoinQuantity(User &user, Group *group) {
    std::string sql = PostgreSQL::prepare("select "
                                          "c.acronym as acronym, "
                                          "(gc.quantity / ( "
                                          "select count(uq) from \"quota\" as q "
                                          "join \"user_has_quota\" uq on uq.quota_id = q.id "
                                          "join \"quota_has_group\" qg on qg.quota_id = q.id "
                                          ") * count(uq)) as quantity "
                                          "from \"quota\" as q "
                                          "join \"user_has_quota\" uq on uq.quota_id = q.id "
                                          "join \"quota_has_group\" qg on qg.quota_id = q.id "
                                          "join \"group\" g on g.id = qg.group_id "
                                          "join \"group_has_coin\" gc on gc.group_id = g.id "
                                          "join \"coin\" c on c.id = gc.coin_id "
                                          "where uq.user_id = ? and g.id = ? "
                                          "group by c.acronym, gc.quantity, g.id;", { std::to_string(user.id), std::to_string(group->id) });

    PostgreSQL *connection = new PostgreSQL("localhost", 5432, "CryptoHolders");
    auto result = connection->execute(sql);

    std::map<std::string, double> coinQuantity;

    for(auto line : result->fetchAll()){
        coinQuantity[line["acronym"]]  = Math::toDouble(line["quantity"]);
    }

    return coinQuantity;
}

std::vector<Coin> Coin::all() {
    std::string sql = "SELECT id, name, LOWER(acronym) as acronym FROM \"coin\" as c ";

    PostgreSQL *connection = new PostgreSQL("localhost", 5432, "CryptoHolders");
    auto result = connection->execute(sql);

    std::vector<Coin> coins;

    for(auto line : result->fetchAll()){
        Coin coin;
        coin.id = Math::toInt(line["id"]);
        coin.name = line["name"];
        coin.acronym = line["acronym"];

        coins.push_back(coin);
    }
    return coins;
}

Cotation Coin::getCurrentCotation() {
    Http::Request request;
    std::string response = request.perform("https://api.bitfinex.com/v1/pubticker/" + this->acronym + "btc");

    nlohmann::json market = nlohmann::json::parse(response);

    Cotation currentMarket;
    currentMarket.lower = Math::toDouble(market["low"]);
    currentMarket.higher = Math::toDouble(market["high"]);
    currentMarket.coin_id = this->id;
    currentMarket.date = *Date::now();
    return currentMarket;
}