//
// Created by pedro on 15/12/17.
//

#ifndef TIGREFRAMEWORK_COIN_H
#define TIGREFRAMEWORK_COIN_H

#include <Types/Date.h>
#include <vector>
#include <string>
#include "User.h"
#include "Cotation.h"

class Group;

class Coin {


    public:
        static std::vector<Coin> getUserCoin(User & user);
        static std::map<std::string, double> getUserCoinQuantity(User & user, Group *group);
        static std::vector<Coin> all();
        static Coin first(int id);

        Cotation getCurrentCotation();

        std::vector<Cotation> getCotation();

        int id;
        std::string name;
        std::string acronym;
};


#endif //TIGREFRAMEWORK_COIN_H
