//
// Created by pedro on 19/12/17.
//

#include <TigreFramework/PostgreSQLConnector/PostgreSQL.h>
#include <Types/Date.h>
#include <Types/Math.h>
#include "Cotation.h"

Cotation::Cotation() : date(*Date::now()) {

}

Cotation::~Cotation() {
    //delete this->date;
}

std::vector<Cotation> Cotation::getCotation(int coin_id) {
    std::string sql = PostgreSQL::prepare("SELECT * FROM \"coin_cotation\" as c "
                                                  "WHERE c.coin_id = ? order by \"date\" asc;", { coin_id });

    PostgreSQL *connection = new PostgreSQL("localhost", 5432, "CryptoHolders");
    auto result = connection->execute(sql);

    std::vector<Cotation> coins;

    for(auto line : result->fetchAll()){
        Cotation coin = Cotation();
        coin.coin_id = Math::toInt(line["coin_id"]);
        coin.lower = Math::toDouble(line["lower"]);
        coin.date = *Date::fromFormat("%Y-%m-%d %H:%M:%S", line["date"]);
        coin.higher = Math::toDouble(line["higher"]);

        coins.push_back(coin);
    }
    return coins;
}

void Cotation::save() {
    std::string sql = PostgreSQL::prepare("INSERT INTO \"coin_cotation\" (coin_id, lower, higher, date) values (?, ?, ?, ?);",
                                          { coin_id, lower, higher, date.format("%Y-%m-%d") });
    PostgreSQL *connection = new PostgreSQL("localhost", 5432, "CryptoHolders");
    connection->execute(sql);
}

void to_json(nlohmann::json &j, const Cotation &p){
    j["coin_id"] = p.coin_id;
    j["lower"] = p.lower;
    j["date"] = p.date.format("%d/%m/%Y");
    j["higher"] = p.higher;
}
