#include <App/http/Controllers/IndexController.h>
#include <App/http/Controllers/Auth/LoginController.h>
#include <App/http/Controllers/Dashboard/DashboardController.h>
#include "Routes.h"

void Routes::path(){
    this->GET("/", Routes::bind(&IndexController::index, IndexController::instance));
    this->GET("/Login", Routes::bind(&LoginController::index, LoginController::instance));
    this->POST("/Login", Routes::bind(&LoginController::logIn, LoginController::instance));
    this->GET("/Dashboard", Routes::bind(&DashboardController::index, DashboardController::instance));
    this->GET("/Dashboard/Logout", Routes::bind(&DashboardController::logOut, DashboardController::instance));
}