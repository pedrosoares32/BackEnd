//
// Created by pedro on 11/12/17.
//

#include <TigreFramework/Core/Kernel/Request.h>
#include <TigreFramework/Core/Kernel/storage/Session.h>
#include <TigreFramework/String/hmac.hpp>
#include <App/Entities/User.h>
#include "LoginController.h"
#include "auth/login.hpp"

LoginController::LoginController(){
    Session *instance = Session::getInstance();
    std::string email = instance->getSession("auth");
    if(email != "") {
        Request::instance->header->clear();
        Request::instance->header->setHeader("HTTP/1.1 302 Found");
        Request::instance->header->setHeader("Location: http://localhost:88/Dashboard");
        throw runtime_error("Usuário já está autenticado");
    }
}

View * LoginController::index() {
    return new class login();
}

View* LoginController::logIn() {
    std::string email = Request::instance->Post("email");
    std::string password = Request::instance->Post("password");

    View* view = new class login();

    User user;
    if(user.getUserByEmail(email) && sign(password, std::to_string(password.length())) == user.password){
        Session *instance = Session::getInstance();
        instance->setSession("auth", email);

        view->with("success", "Login bem sucedido");

        Request::instance->header->clear();
        Request::instance->header->setHeader("HTTP/1.1 302 Found");
        Request::instance->header->setHeader("Location: http://localhost:88/Dashboard");

        return new View("");
    }else{
        view->with("error", "Usuário ou Senha não encontrados");
    }

    return view;
}