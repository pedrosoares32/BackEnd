//
// Created by pedro on 11/12/17.
//

#ifndef TIGREFRAMEWORK_LOGINCONTROLLER_H
#define TIGREFRAMEWORK_LOGINCONTROLLER_H

#include <TigreFramework/Core/Kernel/View.h>
#include "../Controller.h"

class LoginController : public Controller {

    Controller(LoginController);

    public:
        LoginController();
        View * index();
        View * logIn();

};


#endif //TIGREFRAMEWORK_LOGINCONTROLLER_H
