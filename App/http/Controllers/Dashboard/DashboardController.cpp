//
// Created by pedro on 12/12/17.
//

#include <TigreFramework/Core/Kernel/storage/Session.h>
#include <Http/Request.h>
#include <App/Entities/Coin.h>
#include <Types/helper.h>
#include <TigreFramework/Core/Kernel/Request.h>
#include <App/Entities/Group.h>
#include <TigreFramework/String/hmac.hpp>
#include "DashboardController.h"
#include "dashboard/dashboard.hpp"
#include "dashboard/trocar_senha.hpp"

DashboardController::DashboardController() {
    Session *instance = Session::getInstance();
    std::string email = instance->getSession("auth");

    User user;
    if(email == "" || !user.getUserByEmail(email)){
        Request::instance->header->clear();
        Request::instance->header->setHeader("HTTP/1.1 302 Found");
        Request::instance->header->setHeader("Location: http://ch.pedrosoares.net/Login");
        throw runtime_error("Usuário não autenticado");
    }

    this->user = user;
}

View* DashboardController::index() {
    std::vector<Group> groups = Group::getGroupsCoins(this->user);

    Http::Request request;

    Date * date = Date::now();
    date->addDays(-30);

    auto dashboardView = new class dashboard();

    try {
        nlohmann::json cotation = nlohmann::json::parse(
                request.perform("https://api.blinktrade.com/api/v1/BRL/ticker?crypto_currency=BTC")
        );
        dashboardView->with("bitcoinCotation", (double)cotation["last"]);
    }catch (...){

    }

    dashboardView->with("groups", groups);
    dashboardView->with("date", date);

    return dashboardView;
}

View* DashboardController::logOut() {
    Session *instance = Session::getInstance();
    instance->delSession("auth");
    Request::instance->header->clear();
    Request::instance->header->setHeader("HTTP/1.1 302 Found");
    Request::instance->header->setHeader("Location: http://ch.pedrosoares.net/Login");
    throw runtime_error("Saiu com sucesso!");
}

View* DashboardController::trocarSenha() {
    return new class trocar_senha();
}

View* DashboardController::postTrocarSenha() {
    View *response = new class trocar_senha();

    Request *request = Request::instance;
    std::string password = request->Post("password");
    std::string rpassword = request->Post("password");

    if(password == ""){
        response->with("error", "Sua senha não pode ser em branco!");
        return response;
    }

    if(password != rpassword){
        response->with("error", "Senhas não são iguais!");
        return response;
    }

    User user = this->user;
    user.password = sign(password, std::to_string(password.length()));
    if(user.update()) {
        response->with("success", "Senha Trocada");
        return response;
    }

    response->with("error", "Erro interno!");
    return response;
}
