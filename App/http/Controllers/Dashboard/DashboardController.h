//
// Created by pedro on 12/12/17.
//

#ifndef TIGREFRAMEWORK_DASHBOARDCONTROLLER_H
#define TIGREFRAMEWORK_DASHBOARDCONTROLLER_H


#include <TigreFramework/Core/Kernel/View.h>
#include <App/http/Controllers/Controller.h>
#include <App/Entities/User.h>

class DashboardController  : public Controller {

    Controller(DashboardController);

    public:
        DashboardController();
        View * index();
        View * logOut();

    private:
        User user;

};


#endif //TIGREFRAMEWORK_DASHBOARDCONTROLLER_H
