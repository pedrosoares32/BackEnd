//
// Created by pedro on 05/12/17.
//

#include <TigreFramework/Core/Kernel/storage/Session.h>
#include <App/Entities/User.h>
#include "IndexController.h"
//#include "nlohmann/json.hpp"

#include "index.hpp"

//using json = nlohmann::json;

View* IndexController::index() {
    class View * indexPage = new class index();
    indexPage->with("name", "Pedro Soares");
    return indexPage;
}