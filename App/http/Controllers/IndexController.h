//
// Created by pedro on 05/12/17.
//

#ifndef TIGREFRAMEWORK_INDEXCONTROLLER_H
#define TIGREFRAMEWORK_INDEXCONTROLLER_H


#include <TigreFramework/Core/Kernel/View.h>
#include "Controller.h"

class IndexController : public Controller {

    Controller(IndexController);

    public:
        View * index();

};


#endif //TIGREFRAMEWORK_INDEXCONTROLLER_H
