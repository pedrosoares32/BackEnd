#ifndef CONTROLLER_CLASS_H
#define CONTROLLER_CLASS_H

#include <iostream>

#define Controller(Class)                                                \
public:                                                                  \
    static Class instance() {                                               \
        return Class();                                                  \
    }                                                                    \

using namespace std;

class Controller {

};

#endif