//
// Created by pedro on 19/12/17.
//

#include <iostream>
#include <App/Console/Coin/ImportCoin.h>
#include "ConsoleHandle.h"

void ConsoleHandle::index(std::vector<std::string> params) {

    for(auto param : params){
        if(param == "import:coins"){
            ImportCoin importCoin;
            importCoin.import();
        }
    }

}
