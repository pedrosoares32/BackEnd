//
// Created by pedro on 19/12/17.
//

#include <App/Entities/Coin.h>
#include <Http/Request.h>
#include <Types/Math.h>
#include "ImportCoin.h"

void ImportCoin::import() {
    auto coins = Coin::all();
    for(auto coin : coins){
        std::cout << "Importando " << coin.name << std::endl;

        Http::Request request;
        std::string response = request.perform("https://api.bitfinex.com/v1/pubticker/" + coin.acronym + "btc");

        std::cout << "Cotação " << response << std::endl;

        nlohmann::json market = nlohmann::json::parse(response);

        std::cout << "low " << market["low"] << std::endl;
        std::cout << "high " << market["high"] << std::endl;

        Cotation currentMarket;
        currentMarket.lower = Math::toDouble(market["low"]);
        currentMarket.higher = Math::toDouble(market["high"]);
        currentMarket.coin_id = coin.id;
        currentMarket.date = *Date::now();
        currentMarket.save();
    }
}