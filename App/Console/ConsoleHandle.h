//
// Created by pedro on 19/12/17.
//

#ifndef TIGREFRAMEWORK_CONSOLEHANDLE_H
#define TIGREFRAMEWORK_CONSOLEHANDLE_H


#include <string>
#include <vector>

class ConsoleHandle {

    public:
        void index(std::vector<std::string> params);

};


#endif //TIGREFRAMEWORK_CONSOLEHANDLE_H
