create table users (
    id serial primary key,
    name varchar(250),
    email varchar(300),
    password varchar(100),
    key varchar(200),
    secret varchar(200)
);

create table "quota" (
    id serial primary key,
    value DECIMAL(8,2),
    coin_id int
);

create table "user_has_quota" (
    user_id int,
    quota_id int
);

create table "group" (
    id serial primary key,
    started_at int unique,
    name varchar(250),
    status int
);

create table "quota_has_group" (
    group_id int,
    quota_id int
);

create table "coin" (
    id serial primary key,
    name varchar(250),
    acronym varchar(20)
);

create table "user_has_coin" (
    user_id int,
    coin_id int
);

create table "coin_cotation" (
    coin_id int,
    lower DECIMAL(8,8),
    higher DECIMAL(8,8),
    date timestamp
);
